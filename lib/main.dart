import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:siskasaji_digital/pages/category_page.dart';
import 'package:siskasaji_digital/data_search.dart';
import 'package:siskasaji_digital/pages/favorite_page.dart';
import 'package:siskasaji_digital/splash_screen.dart';
import 'pages/home_page.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
  runApp(MaterialApp(
    home: SplashScreenPage(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SISKASAJI DIGITAL',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePage createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  int _selectedIndex = 0;
  List<Widget> _widgetOptions = <Widget>[
    Home(),
    Category(),
    Favorite(),
  ];

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _bottomNavBarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(
          Icons.home,
        ),
        title: Text(
          'Beranda',
          style: TextStyle(
            fontFamily: 'MontserratMedium',
            fontSize: 10,
          ),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.grid_view),
        title: Text(
          'Kategori',
          style: TextStyle(
            fontFamily: 'MontserratMedium',
            fontSize: 10,
          ),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.favorite),
        title: Text(
          'Favorit',
          style: TextStyle(
            fontFamily: 'MontserratMedium',
            fontSize: 10,
          ),
        ),
      ),
    ];

    final _bottomNavBar = BottomNavigationBar(
      items: _bottomNavBarItems,
      currentIndex: _selectedIndex,
      selectedItemColor: Color.fromRGBO(225, 70, 70, 1),
      unselectedItemColor: Colors.grey,
      onTap: _onItemTap,
    );

    return Scaffold(
      backgroundColor: Color.fromRGBO(225, 70, 70, 1),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(225, 70, 70, 1),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                showSearch(context: context, delegate: DataSearch());
              })
        ],
        elevation: 0.0,
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: _bottomNavBar,
    );
  }
}
