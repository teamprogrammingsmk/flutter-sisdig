import 'dart:ui';

import 'package:flutter/material.dart';

class ArtikelPage extends StatefulWidget {
  @override
  _ArtikelPageState createState() => _ArtikelPageState();
}

class _ArtikelPageState extends State<ArtikelPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(244, 244, 244, 1),
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 254,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0.0, 2.0),
                      blurRadius: 6.0,
                    ),
                  ],
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50.0),
                  ),
                  child: Image(
                    image: AssetImage('assets/images/tes.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 30.0),
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back),
                      iconSize: 30.0,
                      color: Colors.white,
                      onPressed: () => Navigator.pop(context),
                    ),
                  ],
                ),
              ),
              Positioned(
                left: 30.0,
                bottom: 20.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          width: 300,
                          color: Colors.grey.withOpacity(0.2),
                          child: Text(
                            'Cerita tentang Maspion IT Surabaya',
                            style: TextStyle(
                              fontFamily: 'PoppinsMedium',
                              fontSize: 18.0,
                              letterSpacing: 1.2,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                right: 10.0,
                bottom: 0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Container(
                    width: 53,
                    height: 53,
                    color: Colors.white,
                  ),
                ),
              ),
              Positioned(
                right: 10.0,
                bottom: 0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Container(
                    width: 53,
                    height: 53,
                    child: IconButton(
                      icon: Icon(Icons.favorite),
                      color: Colors.grey,
                      onPressed: () {},
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.av_timer,
                        color: Colors.grey,
                        size: 18,
                      ),
                      Text(
                        '8 Desember 2020',
                        style: TextStyle(
                          fontFamily: 'PoppinsMedium',
                          fontSize: 9.0,
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    'awnda cfvkdjnadaowds sjdnjnda csndnfnwjv dfv jdnkjncd vdfjskdjnca scsnfvj',
                    style: TextStyle(
                      fontFamily: 'MontserratMedium',
                      fontSize: 13,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
