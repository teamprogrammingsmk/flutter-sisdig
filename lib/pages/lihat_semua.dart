import 'package:flutter/material.dart';
import 'package:siskasaji_digital/pages/article_page.dart';

import '../data_search.dart';

class LihatSemua extends StatefulWidget {
  @override
  _LihatSemuaState createState() => _LihatSemuaState();
}

class _LihatSemuaState extends State<LihatSemua> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(225, 70, 70, 1),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(225, 70, 70, 1),
        title: Text(
          'Lihat Semua',
          style: TextStyle(fontFamily: 'Poppins', fontSize: 20.0),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                showSearch(context: context, delegate: DataSearch());
              })
        ],
        elevation: 0.0,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              // height: 500.0,
              decoration: BoxDecoration(
                color: Color.fromRGBO(244, 244, 244, 1),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50.0),
                  topRight: Radius.circular(50.0),
                ),
              ),
              child: Column(
                children: <Widget>[
                  Padding(padding: const EdgeInsets.symmetric(vertical: 25)),
                  Expanded(
                    child: GridView.builder(
                      itemCount: 5,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 1,
                        mainAxisSpacing: 10,
                        // crossAxisSpacing: ,
                        childAspectRatio: 2.3,
                      ),
                      itemBuilder: (context, index) => ItemCard(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ItemCard extends StatelessWidget {
  const ItemCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        GestureDetector(
          onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => ArtikelPage(),
            ),
          ),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(40, 2, 40, 2),
                height: 170,
                width: 330,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(160, 20, 20, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            width: 120,
                            child: Text(
                              'Cerita tentang maspion IT Surabaya',
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'PoppinsMedium',
                                fontSize: 14,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Teknologi',
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 9.0,
                              color: Color.fromRGBO(74, 134, 232, 1),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            height: 22,
                            width: 35,
                            child: IconButton(
                              icon: Icon(
                                Icons.favorite_border,
                              ),
                              onPressed: () {
                                showSearch(context: context, delegate: null);
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: 40,
          top: 2,
          bottom: 5,
          child: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
            child: Image(
              width: 130,
              image: AssetImage('assets/images/tes.jpg'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
          left: 40,
          top: 2,
          bottom: 5,
          child: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomLeft: Radius.circular(20),
            ),
            child: Container(
              width: 130,
              height: 170,
              color: Colors.grey.withOpacity(0.3),
            ),
          ),
        ),
        Positioned(
          left: 80,
          top: 10,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(2),
            child: Container(
              width: 78,
              height: 12,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    '8 Desember 2020',
                    style: TextStyle(
                      fontFamily: 'PoppinsMedium',
                      fontSize: 7.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
