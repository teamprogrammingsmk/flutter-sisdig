import 'package:flutter/material.dart';
import 'package:siskasaji_digital/widgets/populer_carousel.dart';
import 'package:siskasaji_digital/widgets/rekomendasi_carousel.dart';
import 'package:siskasaji_digital/widgets/terbaru_carousel.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(225, 70, 70, 1),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              // height: 500.0,
              decoration: BoxDecoration(
                color: Color.fromRGBO(244, 244, 244, 1),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(50.0),
                    topRight: Radius.circular(50.0)),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 40),
                child: SafeArea(
                  child: ListView(
                    children: <Widget>[
                      TerbaruCarousel(),
                      PopulerCarousel(),
                      RekomCarousel(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
