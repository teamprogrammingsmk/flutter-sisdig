import 'package:flutter/material.dart';
import 'dart:async';
import 'package:siskasaji_digital/main.dart';

class MySplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          child: SplashScreenPage(),
        ),
      ),
    );
  }
}

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    super.initState();
    startSplashScreen();
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 5);
    return Timer(duration, () {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) {
          return HomePage();
        }),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomLeft,
            colors: [
              Color.fromRGBO(225, 70, 70, 1),
              Color.fromRGBO(252, 74, 26, 1)
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 0.5, left: 200),
              child: Center(
                child: Image.asset("assets/images/UpFront4.png"),
              ),
            ),
            Container(
              child: Center(
                child: Text(
                  'SISKASAJI DIGITAL',
                  style: TextStyle(
                    fontFamily: 'AlegreyaSansSCBold',
                    fontSize: 32.0,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 0, right: 50),
              child: Center(
                child: Image.asset("assets/images/BottomFront4.png"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
